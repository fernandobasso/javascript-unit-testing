
import {
  post,
  request,
} from './inner-fn';

jest.unmock('./inner-fn');

describe('inner-fn.js', () => {
  it('should be defined', () => {
    expect(post).toBeDefined();
    expect(post.constructor).toEqual(Function);
  });

  it('should call the request method', () => {
    request.mockImplementationOnce(() => 'foo' ); 
    post({ id: 1 });
    expect(request).toHaveBeenCalledWith({ id: 2 });
  });
});
